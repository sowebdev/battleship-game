# The Battleship Game

Small library for building a battleship game in PHP.

[![Minimum PHP Version](https://img.shields.io/badge/php-%3E%3D%207.0-8892BF.svg)](https://php.net/)
[![Build status](https://gitlab.com/sowebdev/battleship-game/badges/master/build.svg)](https://gitlab.com/sowebdev/battleship-game/commits/master)
[![Coverage report](https://gitlab.com/sowebdev/battleship-game/badges/master/coverage.svg)](https://gitlab.com/sowebdev/battleship-game/pipelines)

## Requirements

PHP 7+

## Description

This library only provides the game logic.
Some changes have been made comparing to the [original game](https://en.wikipedia.org/wiki/Battleship_(game)) :
- all players share the same grid
- ships are placed randomly on the map
- all ships have the same size
- each player only has one ship

Any user or networking management is out of scope and will have to be implemented on your side.

## Example usage

```php
<?php

$players = [
  new \Battleship\Game\Player(),
  new \Battleship\Game\Player(),
];
$config = new \Battleship\Game\Configuration($players);
$config->setShipSize(3);

$gameBuilder = new \Battleship\Game\GameBuilder();
$game = $gameBuilder->newGame($config);

$player = $game->currentPlayer();
$game->shoot($player, 0, 1);
if ($game->isOver()) {
    echo "Already over";
}
```
