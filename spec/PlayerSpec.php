<?php

use Battleship\Game\Player;
use Battleship\Game\Ship;

describe(Player::class, function () {

    it('can have one or more ships', function () {
        $player = new Player();

        expect($player->getShips())->toHaveLength(0);

        new Ship($player, 1);
        expect($player->getShips())->toHaveLength(1);

        new Ship($player, 1);
        expect($player->getShips())->toHaveLength(2);
    });

    it('is eliminated when all assigned ships have sunk', function () {
        $player = new Player();
        $firstShip = new Ship($player, 1);
        $secondShip = new Ship($player, 1);
        allow($firstShip)->toReceive('hasSunk')->andReturn(false);
        allow($secondShip)->toReceive('hasSunk')->andReturn(false);

        expect($player->isEliminated())->toBe(false);

        allow($firstShip)->toReceive('hasSunk')->andReturn(true);
        expect($player->isEliminated())->toBe(false);

        allow($secondShip)->toReceive('hasSunk')->andReturn(true);
        expect($player->isEliminated())->toBe(true);
    });

});
