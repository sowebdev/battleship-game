<?php

use Battleship\Game\Map;
use Battleship\Game\MapFactory;

describe(MapFactory::class, function () {

    it('creates a map with given dimensions', function () {
        $mapFactory = new MapFactory();
        $map = $mapFactory->build(5, 10);

        expect($map)->toBeAnInstanceOf(Map::class);
        expect($map)->toMatch(function ($actual) {
            return $actual->getWidth() === 5 && $actual->getHeight() === 10;
        });
    });

});
