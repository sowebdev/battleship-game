<?php
use Battleship\Game\Map\Location;
use Battleship\Game\Map\View\Location as ViewLocation;
use Battleship\Game\Player;
use Battleship\Game\Ship;
use Battleship\Game\Shot;

describe(ViewLocation::class, function() {

    describe('::__construct()', function() {

        it('is initialized with a Player and a Map\Location', function() {
            $player = new Player();
            $location = new Location(0, 0);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation)->toBeAnInstanceOf(ViewLocation::class);
        });

    });

    describe('::getX() and getY()', function() {

        it('has the same coordinates than injected Map\Location', function() {
            $player = new Player();
            $location = new Location(0, 0);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getX())->toBe(0);
            expect($viewLocation->getY())->toBe(0);

            $location = new Location(4, 7);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getX())->toBe(4);
            expect($viewLocation->getY())->toBe(7);
        });

    });

    describe('::getState()', function() {

        it('is not discovered when Map\Location contains neither the player\'s ship, neither one of his shots', function() {
            $player = new Player();
            $location = new Location(0, 0);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getState())->toBe(ViewLocation::STATE_NOT_DISCOVERED);
        });

        it('has hit water if it contains the player\'s shot and nothing else', function() {
            $player = new Player();
            $shot = new Shot($player);
            $location = new Location(0, 0);
            $location->addShot($shot);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getState())->toBe(ViewLocation::STATE_HIT_WATER);
        });

        it('has hit enemy if it contains the player\'s shot and an enemy\'s ship', function() {
            $player = new Player();
            $enemy = new Player();
            $ship = new Ship($enemy, 1);
            $shot = new Shot($player);
            $location = new Location(0, 0);
            $location->setShip($ship);
            $location->addShot($shot);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getState())->toBe(ViewLocation::STATE_HIT_ENEMY);
        });

        it('is own ship if it contains the player\'s ship and no shots', function() {
            $player = new Player();
            $ship = new Ship($player, 1);
            $location = new Location(0, 0);
            $location->setShip($ship);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getState())->toBe(ViewLocation::STATE_OWN_SHIP);
        });

        it('is own ship damaged if it contains the player\'s ship and one or more shots', function() {
            $player = new Player();
            $enemy = new Player();
            $shot = new Shot($enemy);
            $ship = new Ship($player, 1);
            $location = new Location(0, 0);
            $location->setShip($ship);
            $location->addShot($shot);
            $viewLocation = new ViewLocation($player, $location);

            expect($viewLocation->getState())->toBe(ViewLocation::STATE_OWN_SHIP_DAMAGED);
        });

    });

});
