<?php

use Battleship\Game\Map\Location;
use Battleship\Game\Map\LocationCollection;
use Battleship\Game\Player;
use Battleship\Game\Ship;

describe(LocationCollection::class, function () {

    it('is countable and traversable', function () {
        $locationCollection = new LocationCollection();
        expect($locationCollection)->toBeAnInstanceOf(\Countable::class);
        expect($locationCollection)->toBeAnInstanceOf(\Iterator::class);
        expect($locationCollection)->toBeAnInstanceOf(\ArrayAccess::class);
    });

    it('can be initialized with an array of locations', function () {
        $locationCollection = new LocationCollection([
            new Location(0, 0),
            new Location(1, 0),
            new Location(2, 0),
        ]);

        expect($locationCollection)->toHaveLength(3);
    });

    it('cannot be initialized with anything else than locations', function () {
        expect(function () { new LocationCollection([new stdClass()]); })
            ->toThrow(new \InvalidArgumentException());
    });

    it('cannot be initialized with duplicate locations', function () {
        expect(function () { new LocationCollection([
            new Location(0, 0),
            new Location(0, 0),
        ]); })
            ->toThrow(new \InvalidArgumentException());
    });

    it('provides access to a location given its coordinates if it exists', function () {
        $locationCollection = new LocationCollection([
            new Location(3, 4),
            new Location(2, 1),
            new Location(0, 3),
        ]);

        expect($locationCollection->atCoordinates(2, 1))->toMatch(function ($actual) {
            return $actual->getX() === 2 && $actual->getY() === 1;
        });
        expect($locationCollection->atCoordinates(1, 1))->toBeNull();
    });

    it('can filter not occupied locations', function () {
        $l1 = new Location(0, 0);
        $l1->setShip(new Ship(new Player(), 1));
        $l4 = new Location(3, 0);
        $l4->setShip(new Ship(new Player(), 1));
        $l5 = new Location(4, 0);
        $l5->setShip(new Ship(new Player(), 1));

        $l2 = new Location(1, 0);
        $l3 = new Location(2, 0);

        $locationCollection = new LocationCollection([
            $l1,
            $l2,
            $l3,
            $l4,
            $l5,
        ]);

        expect($locationCollection)->toHaveLength(5);
        expect($locationCollection->notOccupied())->toHaveLength(2);
    });

    it('can be shuffled', function () {
        $locations = [
            new Location(0, 0),
            new Location(1, 0),
            new Location(2, 0),
        ];
        $locationCollection = new LocationCollection($locations);

        $shuffledLocations = [
            $locations[1],
            $locations[0],
            $locations[2],
        ];
        allow('shuffle')->toBeCalled()->andReturn($shuffledLocations);

        expect($locationCollection->shuffle())->toMatch(function ($actual) use ($locationCollection) {
            for ($i = 0, $count = count($actual); $i < $count; $i++) {
                if ($actual[$i]->getX() !== $locationCollection[$i]->getX()
                    || $actual[$i]->getY() !== $locationCollection[$i]->getY()
                ) {
                    return false;
                }
            }
            return true;
        });
    });

});
