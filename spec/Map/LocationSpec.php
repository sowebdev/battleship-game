<?php

use Battleship\Game\Map\Location;
use Battleship\Game\Player;
use Battleship\Game\Ship;
use Battleship\Game\Shot;
use Kahlan\Plugin\Double;

describe(Location::class, function () {

    given('location', function () {
        return new Location(4, 1);
    });

    it('is initialized with coordinates', function () {
        expect($this->location->getX())->toBe(4);
        expect($this->location->getY())->toBe(1);
    });

    it('is not occupied by default', function () {
        expect($this->location->isOccupied())->toBe(false);
        expect($this->location->isNotOccupied())->toBe(true);
    });

    it('is occupied when it has a ship', function () {

        $shipStub = Double::instance(['extends' => Ship::class, 'methods' => '__construct']);
        $this->location->setShip($shipStub);

        expect($this->location->isOccupied())->toBe(true);
        expect($this->location->isNotOccupied())->toBe(false);
    });

    describe('receiving shots', function () {

        describe('from different players', function () {

            given('firstShot', function () {
                return new Shot(new Player());
            });

            given('secondShot', function () {
                return new Shot(new Player());
            });

            it('holds references to all received shots', function () {
                expect($this->location->hasShots())->toBe(false);
                expect($this->location->getShots())->toHaveLength(0);

                $this->location->addShot($this->firstShot);

                expect($this->location->hasShots())->toBe(true);
                expect($this->location->getShots())->toHaveLength(1);

                $this->location->addShot($this->secondShot);

                expect($this->location->hasShots())->toBe(true);
                expect($this->location->getShots())->toHaveLength(2);
            });

        });

        describe('from a same player', function () {

            given('player', function () {
                return new Player();
            });

            given('firstShot', function () {
                return new Shot($this->player);
            });

            given('secondShot', function () {
                return new Shot($this->player);
            });

            it('allows a player to shoot it only once', function () {
                expect($this->location->canReceiveShot($this->firstShot))->toBe(true);

                $this->location->addShot($this->firstShot);

                expect($this->location->canReceiveShot($this->secondShot))->toBe(false);
                expect(function () { $this->location->addShot($this->secondShot); })->toThrow(new \LogicException);
            });

        });

    });

});
