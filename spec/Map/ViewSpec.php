<?php

use Battleship\Game\Map;
use Battleship\Game\Map\View;
use Battleship\Game\MapFactory;
use Battleship\Game\Player;
use Kahlan\Plugin\Double;

describe(View::class, function() {

    describe('::__construct()', function() {

        given('map', function () {
            $mapFactory = new MapFactory();
            return $mapFactory->build(10, 10);
        });

        it('is initialized with a Player and a Map\Location', function() {
            $view = new View(new Player(), $this->map);

            expect($view)->toBeAnInstanceOf(View::class);
        });

    });

    describe('::getWidth() and getHeight()', function() {

        given('map', function () {
            $mapFactory = new MapFactory();
            return $mapFactory->build(4, 6);
        });

        it('has the same size than injected Map', function() {
            $view = new View(new Player(), $this->map);

            expect($view->getWidth())->toBe(4);
            expect($view->getHeight())->toBe(6);
        });

    });

    describe('::getLocation()', function() {

        it('builds view a location using the map\'s location at given coordinates', function() {
            $location = Double::instance(['extends' => Map\Location::class, 'methods' => '__construct']);
            $locationCollection = Double::instance(['extends' => Map\LocationCollection::class]);
            allow($locationCollection)->toReceive('atCoordinates')->with(0, 0)->andReturn($location);
            $map = Double::instance(['extends' => Map::class, 'methods' => '__construct']);
            allow($map)->toReceive('getLocationCollection')->andReturn($locationCollection);
            $view = new View(new Player(), $map);

            expect($view->getLocation(0, 0))->toBeAnInstanceOf(View\Location::class);
        });

    });

});
