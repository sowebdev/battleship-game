<?php

use Battleship\Game\Configuration;
use Battleship\Game\Player;

describe(Configuration::class, function () {

    it('is initialized with an array of at least two players', function () {
        $players = [new Player(), new Player()];
        $config = new Configuration($players);

        expect($config->getPlayers())->toBe($players);

        $playersInvalid = [new Player()];

        expect(function () use ($playersInvalid) {
            new Configuration($playersInvalid);
        })
            ->toThrow(new \InvalidArgumentException());

        $notPlayers = [new stdClass(), new stdClass()];

        expect(function () use ($notPlayers) {
            new Configuration($notPlayers);
        })
            ->toThrow(new \InvalidArgumentException());
    });

    it('has a default ship size', function () {
        $players = [new Player(), new Player()];
        $config = new Configuration($players);

        expect($config->getShipSize())->toBe(3);
    });

    it('enables to configure ship size', function () {
        $players = [new Player(), new Player()];
        $config = new Configuration($players);

        $config->setShipSize(6);
        expect($config->getShipSize())->toBe(6);
    });

});
