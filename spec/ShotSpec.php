<?php

use Battleship\Game\Map\Location;
use Battleship\Game\Player;
use Battleship\Game\Shot;

describe(Shot::class, function () {

    it('is initialized with a player', function () {
        $player = new Player();
        $shot = new Shot($player);

        expect($shot->getPlayer())->toBe($player);
    });

    describe('it can be assigned to a location', function () {

        given('shot', function () {
            $player = new Player();
            return new Shot($player);
        });

        given('location', function () {
            return new Location(0, 0);
        });

        it('will call addShot() to register itself on the location', function () {
            expect($this->location)->toReceive('addShot')->with($this->shot);

            $this->shot->setLocation($this->location);
        });

        it('is not assigned to a location by default', function () {
            expect($this->shot->getLocation())->toBeNull();
        });

    });

});
