<?php

use Battleship\Game\Game;
use Battleship\Game\MapFactory;
use Battleship\Game\Player;
use Battleship\Game\Ship;

describe(Game::class, function () {

    given('player1', function () {
        return new Player();
    });

    given('player2', function () {
        return new Player();
    });

    given('map', function () {
        /**
         * Let this be the example map
         *
         * A : Player 1
         * B : Player 2
         *
         *     0   1   2   3
         *   |---|---|---|---|
         * 0 | A |   |   |   |
         *   |---|---|---|---|
         * 1 |   |   |   |   |
         *   |---|---|---|---|
         * 2 |   |   |   |   |
         *   |---|---|---|---|
         * 3 |   |   |   | B |
         *   |---|---|---|---|
         */

        $shipPlayer1 = new Ship($this->player1, 1, 1);
        $shipPlayer2 = new Ship($this->player2, 1, 1);

        $mapFactory = new MapFactory();
        $this->map = $mapFactory->build(4, 4);

        $shipPlayer1->setLocations([$this->map->getLocationCollection()->atCoordinates(0, 0)]);
        $shipPlayer2->setLocations([$this->map->getLocationCollection()->atCoordinates(1, 1)]);

        return $this->map;
    });

    given('game', function () {
        return new Game([$this->player1, $this->player2], $this->map);
    });


    it('is initialized with players and a map', function () {
        expect($this->game)->toBeAnInstanceOf(Game::class);
    });

    it('is over when only one player is still alive', function () {
        expect($this->game->isOver())->toBe(false);

        $this->game->shoot($this->player1, 1, 1);

        expect($this->game->isOver())->toBe(true);
    });

    it('changes current player after each turn', function () {
        expect($this->game->currentPlayer())->toBe($this->player1);

        $this->game->shoot($this->player1, 1, 0);

        expect($this->game->currentPlayer())->toBe($this->player2);

        $this->game->shoot($this->player2, 0, 1);

        expect($this->game->currentPlayer())->toBe($this->player1);
    });

    it('does not allow to shoot when game is over', function () {
        $this->game->shoot($this->player1, 1, 1);

        expect(function () {
            $this->game->shoot($this->player2, 0, 0);
        })
            ->toThrow(new \LogicException());
    });

    it('does not allow a player to shoot a same location twice', function () {
        $this->game->shoot($this->player1, 1, 0);
        $this->game->shoot($this->player2, 0, 1);

        expect(function () {
            $this->game->shoot($this->player1, 1, 0);
        })
            ->toThrow(new \LogicException());
    });

    it('does not allow a player to shoot a location that is not on the map', function () {
        expect(function () {
            $this->game->shoot($this->player1, 4, 0);
        })
            ->toThrow(new \OutOfBoundsException());
    });

    it('does not allow a player to shoot when it is not his turn', function () {
        expect(function () {
            $this->game->shoot($this->player2, 1, 0);
        })
            ->toThrow(new \LogicException());
    });

    it('does not allow a player to shoot his own ship', function () {
        expect(function () {
            $this->game->shoot($this->player1, 0, 0);
        })
            ->toThrow(new \LogicException());
    });

});
