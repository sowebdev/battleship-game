<?php

use Battleship\Game\Map\Location;
use Battleship\Game\Player;
use Battleship\Game\Ship;

describe(Ship::class, function () {

    it('is initialized with a player and a size', function () {
        $size = 3;
        $player = new Player();

        expect($player)->toReceive('addShip');
        $ship = new Ship($player, $size);

        expect($ship->getSize())->toBe($size);
        expect($ship->getPlayer())->toBe($player);
    });

    describe('::setLocations()', function () {

        given('ship', function () {
            return new Ship(new Player(), 3);
        });

        it('can be assigned to locations', function () {
            $firstLocation = new Location(0, 0);
            expect($firstLocation)->toReceive('setShip');
            $secondLocation = new Location(1, 0);
            expect($secondLocation)->toReceive('setShip');
            $thirdLocation = new Location(2, 0);
            expect($thirdLocation)->toReceive('setShip');
            $locations = [
                $firstLocation,
                $secondLocation,
                $thirdLocation,
            ];

            $this->ship->setLocations($locations);

            expect($this->ship->getLocations())->toBe($locations);
        });

        it('throws an exception when assigned location count does not match its size', function () {
            expect(function () { $this->ship->setLocations([new Location(0, 0)]); })
                ->toThrow(new \LengthException());
            expect(function () {
                $this->ship->setLocations([
                    new Location(0, 0),
                    new Location(1, 0),
                    new Location(2, 0),
                    new Location(3, 0),
                ]);
            })
                ->toThrow(new \LengthException());
        });

    });

    describe('::hasSunk()', function () {

        given('firstLocation', function () {
            return new Location(0, 0);
        });

        given('secondLocation', function () {
            return new Location(0, 0);
        });

        given('ship', function () {
            $ship =  new Ship(new Player(), 2);
            $ship->setLocations([$this->firstLocation, $this->secondLocation]);
            return $ship;
        });

        it('should sink when all its locations were shot', function () {
            allow($this->firstLocation)->toReceive('hasShots')->andReturn(false);
            allow($this->secondLocation)->toReceive('hasShots')->andReturn(false);
            expect($this->ship->hasSunk())->toBe(false);

            allow($this->firstLocation)->toReceive('hasShots')->andReturn(true);
            expect($this->ship->hasSunk())->toBe(false);

            allow($this->secondLocation)->toReceive('hasShots')->andReturn(true);
            expect($this->ship->hasSunk())->toBe(true);
        });
    
    });

});
