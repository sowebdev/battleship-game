<?php

use Battleship\Game\Configuration;
use Battleship\Game\Game;
use Battleship\Game\GameBuilder;
use Battleship\Game\MapFactory;
use Battleship\Game\Player;
use Battleship\Game\Ship;

describe(GameBuilder::class, function () {

    it('creates a game base on a configuration object', function () {
        $players = [new Player(), new Player()];
        $config = new Configuration($players);
        $gameBuilder = new GameBuilder();

        expect($gameBuilder->newGame($config))->toBeAnInstanceOf(Game::class);
    });

    it('can place a ship on the map', function () {
        $mapFactory = new MapFactory();
        $map = $mapFactory->build(3, 3);
        $ship = new Ship(new Player(), 3);
        $gameBuilder = new GameBuilder();

        expect($ship)->toReceive('setLocations')->with(\Kahlan\Arg::toBeAn('array'));

        $gameBuilder->placeShip($ship, $map);
    });

    it('throws an exception if ship cannot be placed on the map', function () {
        $mapFactory = new MapFactory();
        $map = $mapFactory->build(3, 3);
        $ship = new Ship(new Player(), 4);
        $gameBuilder = new GameBuilder();

        expect(function () use ($gameBuilder, $ship, $map) { $gameBuilder->placeShip($ship, $map); })
            ->toThrow(new \RuntimeException());
    });

});
