<?php

use Battleship\Game\Map;
use Battleship\Game\Map\Location;
use Battleship\Game\Map\LocationCollection;

describe(Map::class, function () {

    it('is initialized with a size and a collection of locations', function () {
        $locationCollection = new LocationCollection();
        $map = new Map(15, 20, $locationCollection);

        expect($map->getWidth())->toBe(15);
        expect($map->getHeight())->toBe(20);
        expect($map->getLocationCollection())->toBe($locationCollection);
    });

    describe('::nextLocation()', function () {

        given('locationCollection', function () {
            $locations = [];
            for ($x = 0; $x < 5; $x++) {
                for ($y = 0; $y < 5; $y++) {
                    $locations[] = new Location($x, $y);
                }
            }
            return new LocationCollection($locations);
        });

        given('map', function () {
            return new Map(5, 5, $this->locationCollection);
        });

        it('relies on LocationCollection::atCoordinates() to retrieve matching location', function () {
            $location = new Location(2, 2);

            expect($this->locationCollection)->toReceive('atCoordinates')->with(2, 1);
            $this->map->nextLocation($location, Map::LOOK_UP);

            expect($this->locationCollection)->toReceive('atCoordinates')->with(2, 3);
            $this->map->nextLocation($location, Map::LOOK_DOWN);

            expect($this->locationCollection)->toReceive('atCoordinates')->with(1, 2);
            $this->map->nextLocation($location, Map::LOOK_LEFT);

            expect($this->locationCollection)->toReceive('atCoordinates')->with(3, 2);
            $this->map->nextLocation($location, Map::LOOK_RIGHT);
        });

        it('throws an exception when direction is unknown', function () {
            expect(function () {
                $this->map->nextLocation(new Location(0, 0), 'good luck with that');
            })->toThrow(new \InvalidArgumentException());
        });
    
    });

});
