<?php

namespace Battleship\Game;

class Configuration
{
    /**
     * @var Player[]
     */
    protected $players;

    /**
     * Default ship size
     *
     * @var int
     */
    protected $shipSize = 3;

    /**
     * @param array $players
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $players)
    {
        if (count($players) < 2) {
            throw new \InvalidArgumentException('Game should be built with at least two players');
        }
        foreach ($players as $player) {
            if (!$player instanceof Player) {
                throw new \InvalidArgumentException('Player should be an instance of ' . Player::class);
            }
        }
        $this->players = $players;
    }

    /**
     * @return Player[]
     */
    public function getPlayers() : array
    {
        return $this->players;
    }

    /**
     * @return int
     */
    public function getShipSize() : int
    {
        return $this->shipSize;
    }

    /**
     * @param int $shipSize
     *
     * @return $this
     */
    public function setShipSize(int $shipSize)
    {
        $this->shipSize = $shipSize;

        return $this;
    }
}
