<?php

namespace Battleship\Game;

use Battleship\Game\Map\Location;

class Ship
{
    /**
     * @var Player
     */
    protected $player;

    /**
     * @var int
     */
    protected $size;

    /**
     * @var Location[]
     */
    protected $locations = [];

    /**
     * @param Player $player
     * @param int    $size
     */
    public function __construct(Player $player, int $size)
    {
        $this->player = $player;
        $this->size = $size;

        $this->player->addShip($this);
    }

    /**
     * @return Player
     */
    public function getPlayer() : Player
    {
        return $this->player;
    }

    /**
     * @return int
     */
    public function getSize() : int
    {
        return $this->size;
    }

    /**
     * @param Location[] $locations
     */
    public function setLocations(array $locations)
    {
        if (count($locations) != $this->size) {
            throw new \LengthException("Number of locations doesn't match the size of the ship");
        }
        foreach ($locations as $location) {
            $location->setShip($this);
        }
        $this->locations = $locations;
    }

    /**
     * @return Location[]
     */
    public function getLocations() : array
    {
        return $this->locations;
    }

    /**
     * @return bool
     */
    public function hasSunk() : bool
    {
        foreach ($this->locations as $location) {
            if (!$location->hasShots()) {
                return false;
            }
        }
        return true;
    }
}
