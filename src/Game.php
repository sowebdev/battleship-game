<?php

namespace Battleship\Game;

class Game
{
    /**
     * @var Player[]
     */
    protected $players = [];

    /**
     * @var int
     */
    protected $currentPlayerIndex = 0;

    /**
     * @var Map
     */
    protected $map;

    /**
     * @param array $players
     * @param Map   $map
     */
    public function __construct(array $players, Map $map)
    {
        $this->players = $players;
        $this->map = $map;
    }

    /**
     * @return bool
     */
    public function isOver() : bool
    {
        $countPlayersAlive = count($this->players);
        foreach ($this->players as $player) {
            if ($player->isEliminated()) {
                $countPlayersAlive--;
            }
        }
        return $countPlayersAlive === 1;
    }

    /**
     * @param Player $player
     * @param int    $x
     * @param int    $y
     *
     * @throws \LogicException
     * @throws \OutOfBoundsException
     */
    public function shoot(Player $player, int $x, int $y)
    {
        if ($this->isOver()) {
            throw new \LogicException('Not allowed to shoot when game is over');
        }

        if ($this->currentPlayer() !== $player) {
            throw new \LogicException('Not allowed to shoot : player should wait his turn');
        }

        $location = $this->map->getLocationCollection()->atCoordinates($x, $y);
        if (!$location) {
            throw new \OutOfBoundsException('Not allowed to shoot outside the map');
        }
        if ($location->isOccupied() && $location->getShip()->getPlayer() === $player) {
            throw new \LogicException('Not allowed to shoot own ship');
        }

        foreach ($location->getShots() as $shot) {
            if ($shot->getPlayer() === $player) {
                throw new \LogicException('Not allowed to shoot same location twice');
            }
        }

        $shot = new Shot($player);
        $shot->setLocation($location);

        $this->changeCurrentPlayer();
    }

    /**
     * @return Player
     */
    public function currentPlayer() : Player
    {
        return $this->players[$this->currentPlayerIndex];
    }

    protected function changeCurrentPlayer()
    {
        $this->currentPlayerIndex++;
        if ($this->currentPlayerIndex >= count($this->players)) {
            $this->currentPlayerIndex = 0;
        }
    }
}
