<?php

namespace Battleship\Game;

class Player
{
    /**
     * @var Ship[]
     */
    protected $ships = [];

    /**
     * @return Ship[]
     */
    public function getShips() : array
    {
        return $this->ships;
    }

    /**
     * @param Ship $ship
     */
    public function addShip(Ship $ship)
    {
        $this->ships[] = $ship;
    }

    /**
     * @return bool
     */
    public function isEliminated() : bool
    {
        if (!count($this->ships)) {
            return false;
        }
        foreach ($this->ships as $ship) {
            if (! $ship->hasSunk()) {
                return false;
            }
        }
        return true;
    }
}
