<?php

namespace Battleship\Game;

class GameBuilder
{
    /**
     * @param Configuration $config
     *
     * @return Game
     */
    public function newGame(Configuration $config) : Game
    {
        $mapFactory = new MapFactory();
        $map = $mapFactory->build(10, 10);
        
        foreach ($config->getPlayers() as $player) {
            $ship = new Ship($player, $config->getShipSize());
            $this->placeShip($ship, $map);
        }
        
        return new Game($config->getPlayers(), $map);
    }

    /**
     * @param Ship $ship
     * @param Map  $map
     *
     * @throws \RuntimeException
     */
    public function placeShip(Ship $ship, Map $map)
    {
        $freeStartLocations = $map->getLocationCollection()->notOccupied()->shuffle();
        $directions = [
            Map::LOOK_LEFT,
            Map::LOOK_RIGHT,
            Map::LOOK_UP,
            Map::LOOK_DOWN
        ];
        shuffle($directions);

        for ($i = 0, $maxI = count($freeStartLocations); $i < $maxI; $i++) {
            for ($dirIndex = 0, $maxDirections = count($directions); $dirIndex < $maxDirections; $dirIndex++) {
                $currentLocation = $freeStartLocations[$i];
                $freeLocationsChecked = [$currentLocation];
                while (count($freeLocationsChecked) < $ship->getSize()) {
                    $nextLocation = $map->nextLocation($currentLocation, $directions[$dirIndex]);
                    if (! $nextLocation || $nextLocation->isOccupied()) {
                        continue 2;
                    }
                    $currentLocation = $nextLocation;
                    $freeLocationsChecked[] = $currentLocation;
                }

                $ship->setLocations($freeLocationsChecked);
                return;
            }
        }

        throw new \RuntimeException('Unable to place ship');
    }
}
