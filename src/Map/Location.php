<?php

namespace Battleship\Game\Map;

use Battleship\Game\Ship;
use Battleship\Game\Shot;

class Location
{
    /**
     * @var int
     */
    protected $x;

    /**
     * @var int
     */
    protected $y;

    /**
     * @var Ship|null
     */
    protected $ship;

    /**
     * @var Shot[]
     */
    protected $shots = [];

    /**
     * Location constructor.
     *
     * @param int $x
     * @param int $y
     */
    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * @return bool
     */
    public function isOccupied() : bool
    {
        return (bool) $this->ship;
    }

    /**
     * @return bool
     */
    public function isNotOccupied() : bool
    {
        return !$this->isOccupied();
    }

    /**
     * @param Ship $ship
     */
    public function setShip(Ship $ship)
    {
        $this->ship = $ship;
    }

    /**
     * @return int
     */
    public function getX() : int
    {
        return $this->x;
    }

    /**
     * @return int
     */
    public function getY() : int
    {
        return $this->y;
    }

    /**
     * @return array
     */
    public function getShots() : array
    {
        return $this->shots;
    }

    /**
     * @return bool
     */
    public function hasShots() : bool
    {
        return (bool) $this->shots;
    }

    /**
     * @param Shot $shot
     *
     * @throws \LogicException
     */
    public function addShot(Shot $shot)
    {
        if ($this->canNotReceiveShot($shot)) {
            throw new \LogicException('A player can\'t shoot a same location twice');
        }
        $this->shots[] = $shot;
    }

    /**
     * @param Shot $shot
     *
     * @return bool
     */
    public function canReceiveShot(Shot $shot) : bool
    {
        foreach ($this->shots as $assignedShot) {
            if ($assignedShot->getPlayer() === $shot->getPlayer()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param Shot $shot
     *
     * @return bool
     */
    public function canNotReceiveShot(Shot $shot) : bool
    {
        return !$this->canReceiveShot($shot);
    }

    /**
     * @return Ship|null
     */
    public function getShip()
    {
        return $this->ship;
    }
}
