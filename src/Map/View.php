<?php

namespace Battleship\Game\Map;

use Battleship\Game\Map;
use Battleship\Game\Player;

class View
{
    /**
     * @var Map
     */
    protected $map;

    /**
     * @var Player
     */
    protected $player;

    /**
     * @param Player $player
     * @param Map    $map
     */
    public function __construct(Player $player, Map $map)
    {
        $this->map = $map;
        $this->player = $player;
    }

    /**
     * @return int
     */
    public function getHeight() : int
    {
        return $this->map->getHeight();
    }

    /**
     * @return int
     */
    public function getWidth() : int
    {
        return $this->map->getWidth();
    }

    /**
     * @param int $x
     * @param int $y
     *
     * @return View\Location
     */
    public function getLocation(int $x, int $y) : View\Location
    {
        return new View\Location($this->player, $this->map->getLocationCollection()->atCoordinates($x, $y));
    }
}
