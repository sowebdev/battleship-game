<?php

namespace Battleship\Game\Map;

class LocationCollection implements \Countable, \Iterator, \ArrayAccess
{
    /**
     * @var Location[]
     */
    protected $locations;

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * @var Location[]
     */
    protected $indexByCoordinates = [];

    /**
     * @param Location[] $items
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            if (! $item instanceof Location) {
                throw new \InvalidArgumentException('Location collection expects only Locations');
            }
            if (isset($this->indexByCoordinates[$item->getX() . '-' . $item->getY()])) {
                throw new \InvalidArgumentException('Received locations with duplicate coordinates');
            }
            $this->indexByCoordinates[$item->getX() . '-' . $item->getY()] = $item;
        }

        $this->locations = $items;
    }

    /**
     * @param int $x
     * @param int $y
     *
     * @return Location|null
     */
    public function atCoordinates(int $x, int $y)
    {
        if (isset($this->indexByCoordinates[$x . '-' . $y])) {
            return $this->indexByCoordinates[$x . '-' . $y];
        }
        return null;
    }

    /**
     * @return LocationCollection
     */
    public function notOccupied() : LocationCollection
    {
        $notOccupied = [];
        foreach ($this->locations as $location) {
            if ($location->isNotOccupied()) {
                $notOccupied[] = $location;
            }
        }
        return new static($notOccupied);
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->locations);
    }

    /**
     * @return Location
     */
    public function current() : Location
    {
        return $this->locations[$this->position];
    }

    /**
     *
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * @return int
     */
    public function key() : int
    {
        return $this->position;
    }

    /**
     * @return bool
     */
    public function valid() : bool
    {
        return isset($this->locations[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset) : bool
    {
        return isset($this->locations[$offset]);
    }

    /**
     * @param mixed $offset
     *
     * @return Location
     */
    public function offsetGet($offset) : Location
    {
        return $this->locations[$offset];
    }

    /**
     * @param mixed $offset
     * @param Location $value
     *
     * @throws \InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        if (! $value instanceof Location) {
            throw new \InvalidArgumentException('Expected an instance of ' . Location::class);
        }
        $this->locations[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->locations[$offset]);
    }

    /**
     * @return LocationCollection
     */
    public function shuffle() : LocationCollection
    {
        $copy = $this->locations;
        shuffle($copy);
        return new static($copy);
    }
}
