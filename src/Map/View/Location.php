<?php

namespace Battleship\Game\Map\View;

use Battleship\Game\Player;

class Location
{
    const STATE_NOT_DISCOVERED = 'not_discovered';
    const STATE_HIT_ENEMY = 'hit_enemy';
    const STATE_HIT_WATER = 'hit_water';
    const STATE_OWN_SHIP = 'own_ship';
    const STATE_OWN_SHIP_DAMAGED = 'own_ship_damaged';

    /**
     * @var Player
     */
    protected $player;

    /**
     * @var \Battleship\Game\Map\Location
     */
    protected $location;

    /**
     * @param Player                        $player
     * @param \Battleship\Game\Map\Location $location
     */
    public function __construct(Player $player, \Battleship\Game\Map\Location $location)
    {
        $this->player = $player;
        $this->location = $location;
    }

    /**
     * @return int
     */
    public function getX() : int
    {
        return $this->location->getX();
    }

    /**
     * @return int
     */
    public function getY() : int
    {
        return $this->location->getY();
    }

    /**
     * @return string see STATE_xxx constants
     */
    public function getState() : string
    {
        $containsPlayersShip = false;
        $containsShip = false;
        if ($this->location->getShip()) {
            $containsShip = true;
            if ($this->location->getShip()->getPlayer() === $this->player) {
                $containsPlayersShip = true;
            }
        }
        $containsPlayersShot = false;
        $containsShots = false;
        foreach ($this->location->getShots() as $shot) {
            $containsShots = true;
            if ($shot->getPlayer() === $this->player) {
                $containsPlayersShot = true;
            }
        }

        if ($containsPlayersShip) {
            if ($containsShots) {
                $this->state = static::STATE_OWN_SHIP_DAMAGED;
            } else {
                $this->state = static::STATE_OWN_SHIP;
            }
        } elseif ($containsShip && $containsPlayersShot) {
            $this->state = static::STATE_HIT_ENEMY;
        } elseif ($containsPlayersShot) {
            $this->state = static::STATE_HIT_WATER;
        } else {
            $this->state = static::STATE_NOT_DISCOVERED;
        }

        return $this->state;
    }
}
