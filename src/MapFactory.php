<?php

namespace Battleship\Game;

use Battleship\Game\Map\Location;
use Battleship\Game\Map\LocationCollection;

class MapFactory
{
    /**
     * @param int $width
     * @param int $height
     *
     * @return Map
     */
    public function build(int $width, int $height) : Map
    {
        $locations = [];
        for ($x = 0; $x < $width; $x++) {
            for ($y = 0; $y < $height; $y++) {
                $locations[] = new Location($x, $y);
            }
        }
        return new Map($width, $height, new LocationCollection($locations));
    }
}
