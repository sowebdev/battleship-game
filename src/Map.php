<?php

namespace Battleship\Game;

use Battleship\Game\Map\Location;
use Battleship\Game\Map\LocationCollection;

class Map
{
    const LOOK_UP = 'up';
    const LOOK_DOWN = 'down';
    const LOOK_LEFT = 'left';
    const LOOK_RIGHT = 'right';

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var LocationCollection
     */
    protected $locationCollection;

    /**
     * @param int                $width
     * @param int                $height
     * @param LocationCollection $locationCollection
     */
    public function __construct(int $width, int $height, LocationCollection $locationCollection)
    {
        $this->width = $width;
        $this->height = $height;
        $this->locationCollection = $locationCollection;
    }

    /**
     * @return int
     */
    public function getWidth() : int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight() : int
    {
        return $this->height;
    }

    /**
     * @param Location $location
     * @param string   $direction see LOOK_xxx constants
     *
     * @return Location|null
     */
    public function nextLocation(Location $location, $direction)
    {
        switch ($direction) {
            case static::LOOK_UP:
                $x = $location->getX();
                $y = $location->getY() - 1;
                break;
            case static::LOOK_DOWN:
                $x = $location->getX();
                $y = $location->getY() + 1;
                break;
            case static::LOOK_LEFT:
                $x = $location->getX() - 1;
                $y = $location->getY();
                break;
            case static::LOOK_RIGHT:
                $x = $location->getX() + 1;
                $y = $location->getY();
                break;
            default:
                throw new \InvalidArgumentException('Direction is not valid');
        }
        return $this->locationCollection->atCoordinates($x, $y);
    }

    /**
     * @return LocationCollection
     */
    public function getLocationCollection() : LocationCollection
    {
        return $this->locationCollection;
    }
}
