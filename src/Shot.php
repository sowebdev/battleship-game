<?php

namespace Battleship\Game;

use Battleship\Game\Map\Location;

class Shot
{
    /**
     * @var Player
     */
    protected $player;

    /**
     * @var Location
     */
    protected $location;

    /**
     * @param Player $player
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * @return Player
     */
    public function getPlayer() : Player
    {
        return $this->player;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
        $this->location->addShot($this);
    }

    /**
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
